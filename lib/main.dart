import 'package:flutter/material.dart';

void main() {
  runApp(UIApp());
}

class UIApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const _imageUrl =
        'https://www.archanaskitchen.com/images/archanaskitchen/0-Archanas-Kitchen-Recipes/2018/Thai_Papaya_Salad_Recipe_With_Lemon_Peanut_Honey_Dressing_-5.jpg';
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Page'),
      ),
      body: ListView(
        children: const [
          MyCard(
              'ไม่มีผู้ใดที่สมัครรักใคร่ในความเจ็บปวด',
              'หรือเสาะแสวงหาและปรารถนาจะครอบครองความเจ็บปวด นั่นก็เป็นเพราะว่ามันเจ็บปวด...',
              _imageUrl),
          MyCard('1', '1', _imageUrl),
          MyCard('2', '2', _imageUrl),
          MyCard('3', '3', _imageUrl),
        ],
      ),
    );
  }
}

class MyCard extends StatelessWidget {
  final String _text;
  final String _author;
  final String _imageUrl;
  const MyCard(
    this._text,
    this._author,
    this._imageUrl, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(10),
      elevation: 10,
      child: Column(
        children: [
          Container(
            height: 300,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              image: DecorationImage(
                image: NetworkImage(_imageUrl),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            alignment: Alignment.topLeft,
            padding: const EdgeInsets.all(8),
            child: Text(
              _text,
              style: TextStyle(fontSize: 20),
            ),
          ),
          Container(
            alignment: Alignment.bottomRight,
            padding: const EdgeInsets.all(8),
            child: Text(
              '-- $_author',
            ),
          ),
        ],
      ),
    );
  }
}
